package com.example.tictactoe;

public abstract class AIPlayer{
    protected int ROWS = 3;  // number of rows
    protected int COLS = 3;  // number of columns
    private final String CROSS = "X";
    private final String CIRCLE = "O";
    protected String[][] cells; // the board's ROWS-by-COLS array of Cells
    protected String mySymbol, oppSymbol;

    /** Constructor with reference to game board */
    public AIPlayer(String[][] board) {
        cells = board;
    }

    /** Set/change the seed used by computer and opponent */
    public void setSymbol(String seed) {
        this.mySymbol = seed;
        oppSymbol = (mySymbol == CROSS) ? CIRCLE : CROSS;
    }

    /** Abstract method to get next move. Return int[2] of {row, col} */
    abstract int[] move();  // to be implemented by subclasses
}
