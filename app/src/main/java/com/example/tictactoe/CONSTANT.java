package com.example.tictactoe;

public class CONSTANT {
    public static final String CROSS = "X";
    public static final String NOUGHT = "O";
    public static final String EMPTY = "";

    public static final String LOOKUP = "lookup";
    public static final String MINMAX = "minmax";

}
