package com.example.tictactoe;

import java.io.Serializable;

public class GameSettings implements Serializable {
    private String playerSymbol;
    private String aiMode;

    public GameSettings(String playerSymbol, String aiMode) {
        this.playerSymbol = playerSymbol;
        this.aiMode = aiMode;
    }

    public String getPlayerSymbol() {
        return playerSymbol;
    }

    public void setPlayerSymbol(String playerSymbol) {
        this.playerSymbol = playerSymbol;
    }

    public String getAiMode() {
        return aiMode;
    }

    public void setAiMode(String aiMode) {
        this.aiMode = aiMode;
    }
}
