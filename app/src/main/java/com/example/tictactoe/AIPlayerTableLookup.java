package com.example.tictactoe;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

/**
 * Computer move based on simple table lookup of preferences
 */
public class AIPlayerTableLookup extends AIPlayer {

    // Moves {row, col} in order of preferences. {0, 0} at top-left corner
    private int[][] preferredMoves = {
            {1, 1}, {0, 0}, {0, 2}, {2, 0}, {2, 2},
            {0, 1}, {1, 0}, {1, 2}, {2, 1}};

    /** constructor */
    public AIPlayerTableLookup(String[][] board) {
        super(board);
    }

    /** Search for the first empty cell, according to the preferences
     *  Assume that next move is available, i.e., not gameover
     *  @return int[2] of {row, col}
     */
    @Override
    public int[] move() {
        List moves = Arrays.asList(preferredMoves);
        Collections.shuffle(moves);
        for (Object object : moves) {
            int[] move = (int []) object;
            if (cells[move[0]][move[1]].isEmpty()) {
                return move;
            }
        }
        return null;
    }
}