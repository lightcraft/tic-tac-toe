package com.example.tictactoe;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {

    AIPlayer aiPlayer;
    GameSettings gameSettings;
    private Button[][] buttons = new Button[3][3];
    private int roundCount;
    private TextView textViewPlayer, textViewAi;
    private Player player, ai;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        gameSettings = new GameSettings(CONSTANT.NOUGHT, CONSTANT.LOOKUP);

        //Bind text view
        textViewPlayer = findViewById(R.id.text_view_p1);
        textViewAi = findViewById(R.id.text_view_p2);

        //Bind buttons
        for (int i = 0; i < 3; i++) {
            for (int j = 0; j < 3; j++) {
                String buttonId = "button_" + i + j;
                int resID = getResources().getIdentifier(buttonId, "id", getPackageName());
                buttons[i][j] = findViewById(resID);
                buttons[i][j].setOnClickListener(this);
            }
        }
        Button buttonReset = findViewById(R.id.button_reset);
        buttonReset.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                resetGame();
            }
        });

        //Create player & ai object
        player = new Player(gameSettings.getPlayerSymbol());
        ai = new Player(getAiSymbol());
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu, menu);
        return true;
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        switch (resultCode) {
            case (Activity.RESULT_OK):
                gameSettings = (GameSettings) data.getSerializableExtra("settings");
                resetBoard();
                break;
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == R.id.settings) {
            Intent setttings = new Intent(this, Settings.class);
            int result = 0;
            setttings.putExtra("settings", gameSettings);
            setttings.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            startActivityForResult(setttings, result);
        } else {
            return super.onOptionsItemSelected(item);
        }
        return true;
    }

    @Override
    public void onClick(View v) {
        // invalid move
        if (!((Button) v).getText().toString().isEmpty()) {
            return;
        }

        //valid move
        ((Button) v).setText(gameSettings.getPlayerSymbol());

        //increase round
        roundCount++;

        String winner = checkForWin();

        if (doWinStuff(winner)) return;

        aiPlay();

        roundCount++;

        winner = checkForWin();

        if (doWinStuff(winner)) return;
    }

    private boolean doWinStuff(String winner) {
        if (!winner.isEmpty()) {
            if (winner.equals(gameSettings.getPlayerSymbol())) {
                playerWin(1);
                return true;
            } else {
                playerWin(2);
                return true;
            }
        } else if (roundCount >= 9) {
            playerWin(0);
            return true;
        }
        return false;
    }

    private void aiPlay() {
        String[][] field = getField();
        aiPlayer = new AIPlayerMinMax(field);
//        if (gameSettings.getAiMode().equals(CONSTANT.LOOKUP)){
//            aiPlayer =  new AIPlayerTableLookup(field);
//        }
        aiPlayer.setSymbol(getAiSymbol());
        //Get Best Move
        int[] move = aiPlayer.move();

        //Move!
        String buttonId = "button_" + move[0] + move[1];
        int resID = getResources().getIdentifier(buttonId, "id", getPackageName());
        Button button = findViewById(resID);
        button.setText(getAiSymbol());
    }

    private void playerWin(int result) {
        if (result != 0) {
            if (result == 1) {
                player.increaseScore();
                Toast.makeText(this, "You win!", Toast.LENGTH_SHORT).show();
            } else if (result == 2) {
                ai.increaseScore();
                Toast.makeText(this, "You lose!", Toast.LENGTH_SHORT).show();
            }
            updatePointsText();
            resetBoard();
        } else {
            Toast.makeText(this, "Draw!", Toast.LENGTH_SHORT).show();
            resetBoard();
        }
    }

    private void updatePointsText() {
        textViewPlayer.setText("Player : " + player.getScores());
        textViewAi.setText("AI: " + ai.getScores());
    }

    public void resetBoard() {
        for (int i = 0; i < 3; i++) {
            for (int j = 0; j < 3; j++) {
                buttons[i][j].setText("");
            }
        }
        roundCount = 0;
    }

    private String checkForWin() {
        String[][] field = getField();
        String result = "";

        for (int i = 0; i < 3; i++) {
            if (field[i][0].equals(field[i][1]) && field[i][0].equals(field[i][2]) && !field[i][0].isEmpty()) {
                result = field[i][0];
            }
        }

        for (int i = 0; i < 3; i++) {
            if (field[0][i].equals(field[1][i]) && field[0][i].equals(field[2][i]) && !field[0][i].isEmpty()) {
                result = field[0][i];
            }
        }

        if (field[0][0].equals(field[1][1]) && field[0][0].equals(field[2][2]) && !field[0][0].isEmpty()) {
            result = field[1][1];
        }

        if (field[0][2].equals(field[1][1]) && field[0][2].equals(field[2][0]) && !field[0][2].isEmpty()) {
            result = field[1][1];
        }

        return result;
    }

    private String[][] getField() {
        String[][] field = new String[3][3];

        for (int i = 0; i < 3; i++) {
            for (int j = 0; j < 3; j++) {
                field[i][j] = buttons[i][j].getText().toString();
            }
        }
        return field;
    }

    private void resetGame() {
        player = new Player(gameSettings.getPlayerSymbol());
        ai = new Player(getAiSymbol());
        updatePointsText();
        resetBoard();
    }

    private String getAiSymbol() {
        String result = (gameSettings.getPlayerSymbol().equals(CONSTANT.CROSS)) ? CONSTANT.NOUGHT : CONSTANT.CROSS;
        return result;
    }

    @Override
    protected void onPostResume() {
        resetBoard();
        super.onPostResume();
    }
}
