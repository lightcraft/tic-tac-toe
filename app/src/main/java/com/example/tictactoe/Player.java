package com.example.tictactoe;

public class Player {
    private String symbol;
    private int scores;

    public Player(String symbol) {
        this.symbol = symbol;
        this.scores = 0;
    }

    public String getSymbol() {
        return symbol;
    }

    public void setSymbol(String symbol) {
        this.symbol = symbol;
    }

    public int getScores() {
        return scores;
    }

    public void setScores(int scores) {
        this.scores = scores;
    }

    public void increaseScore(){
        this.scores++;
    }
}
