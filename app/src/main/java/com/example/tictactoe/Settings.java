package com.example.tictactoe;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.MenuItem;
import android.widget.CompoundButton;
import android.widget.RadioButton;

public class Settings extends AppCompatActivity {

    private GameSettings gameSettings;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_settings);
    }

    @Override
    protected void onStart() {

        gameSettings = (GameSettings) getIntent().getSerializableExtra("settings");

        //Init Display
        if (gameSettings.getPlayerSymbol().equals(CONSTANT.NOUGHT)) {
            ((RadioButton) findViewById(R.id.radio_nought)).setChecked(true);
        } else {
            ((RadioButton) findViewById(R.id.radio_cross)).setChecked(true);
        }

        //Init Display
        if (gameSettings.getAiMode().isEmpty() || gameSettings.getAiMode().equals(CONSTANT.LOOKUP)) {
            ((RadioButton) findViewById(R.id.ai_lookup)).setChecked(true);
        } else if (gameSettings.getAiMode().equals(CONSTANT.MINMAX)) {
            ((RadioButton) findViewById(R.id.ai_min_max)).setChecked(true);
        }

        //On check Player Symbol
        ((RadioButton) findViewById(R.id.radio_cross)).setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {
                    gameSettings.setPlayerSymbol(CONSTANT.CROSS);
                }
            }
        });

        ((RadioButton) findViewById(R.id.radio_nought)).setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {
                    gameSettings.setPlayerSymbol(CONSTANT.NOUGHT);
                }
            }
        });

        //On Check Difficulty
        ((RadioButton) findViewById(R.id.ai_min_max)).setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {
                    gameSettings.setAiMode(CONSTANT.MINMAX);
                }
            }
        });

        ((RadioButton) findViewById(R.id.ai_lookup)).setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {
                    gameSettings.setAiMode(CONSTANT.LOOKUP);
                }
            }
        });

        super.onStart();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                Intent resultIntent = new Intent();
                resultIntent.putExtra("settings", gameSettings);
                setResult(Activity.RESULT_OK, resultIntent);
                this.finish();
        }
        return true;
    }
}
